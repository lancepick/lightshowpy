import os
import requests
import time
import shlex, subprocess
import sys
import re
import filewalker
import RPi.GPIO as GPIO


baseUrl = 'http://pickemail.net/api/'
lightshowPath = '/home/pi/lightshowpi/py/synchronized_lights.py'
rootMusicPath = '/mnt/usb/'
subprocessCmdTpl = '{0} --file="{1}"'.format(lightshowPath, '{0}')
isSongPlaying = False
proc = None
lightsAreOn = True

def playSong():
  global isSongPlaying
  global proc
  response = requests.get(baseUrl + 'LoadTrack')
  if(response.ok and response.json() is not None and  len(response.json())>0):
    isSongPlaying = True
    proc = subprocess.Popen(shlex.split(subprocessCmdTpl.format(response.json())))
    print 'Fetched From Playlist: ' + response.json()
  else:
    isSongPlaying = False
    time.sleep(2)
  return

def checkKillSwitch():
  response = requests.get(baseUrl + 'KillSwitch')
  ret = response.json()
  return ret==True

def fetchRandomSong():
  response = requests.get(baseUrl + 'LoadRandomSong')
  ret = ''
  if(response.ok and response.json() is not None and  len(response.json())>0):
    ret = response.json()
  print 'Fetched Random Song: ' + ret
  return ret

###===============================================================================
###  FILE WALKER
###===============================================================================
filewalker.syncLibrary(rootMusicPath, baseUrl + 'LibrarySync')

###===============================================================================
###  BUTTON SETUP
###===============================================================================
def buttonPressed(channel):
  global isSongPlaying
  global proc
  print 'ALERT! Button Pushed!'
  song = fetchRandomSong()
  if(len(song)>0):
    if(isSongPlaying):
      proc.kill()
    proc = subprocess.Popen(shlex.split(subprocessCmdTpl.format(song)))
    isSongPlaying = True
  return


#GPIO.setmode(GPIO.BCM)
#GPIO.setup(2, GPIO.IN, pull_up_down = GPIO.PUD_UP)
#GPIO.add_event_detect(2, GPIO.FALLING, callback = buttonPressed, bouncetime = 300)


###===============================================================================
###  Turn Ligths Off/On
###===============================================================================
def turnLightsOn():
  print "Turn Lights ON"
  global lightsAreOn
  lightsAreOn = True
  os.system("/home/pi/lightshowpi/py/hardware_controller.py --state=on")
  return

def turnLightsOff():
  print "Turn Lights OFF"
  global lightsAreOn
  lightsAreOn = False
  os.system("/home/pi/lightshowpi/py/hardware_controller.py --state=off")
  return


###===============================================================================
###  EVENT LOOP
###===============================================================================
while(True):
  try:
    if(isSongPlaying):
      if(proc.poll() is not None):
        turnLightsOn()
        print 'Song Ended'
        isSongPlaying = False
      elif(checkKillSwitch()):
        turnLightsOn()
        print 'Kill Requested'
        isSongPlaying = False
        proc.kill()
      else:
        time.sleep(2)
    else:
      if(lightsAreOn == False):
        turnLightsOn()
      playSong()
  except:
    print "ERROR:", sys.exc_info()
