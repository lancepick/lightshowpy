import os
import re
import requests
import json
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3

#baseUrl = 'http://localhost:64981/'
#rootMusicFolder = 'd:/music'

def syncLibrary(rootMusicFolder, apiUrl):
  fileList = []
  currentLibrary = requests.get(apiUrl).json()
  reMusic = re.compile(r"^.*\.mp3")
  for root, dirs, files in os.walk(rootMusicFolder):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not d[0] == '.']
    for file in files:
      if reMusic.search(file):
        fileList.append(os.path.join(root, file))

  deleteList = []
  for x in currentLibrary:
    found = False
    for y in fileList:
          if(x["filePath"].lower()==y.lower()):
            found = True
            break
    if(found == False):
      deleteList.append(x["trackId"])

  newList = []
  for y in fileList:
    skip = False
    for x in currentLibrary:
      if(x["filePath"].lower()==y.lower()):
        skip = True
        break
    if(skip == False):
      audio = MP3(y)
      audioTime = audio.info.length
      minutes = int(audio.info.length//60)
      seconds = int(audio.info.length-(minutes*60))
      try:
        tags = EasyID3(y)
        newList.append({'trackId' : 0, 'album' : tags["album"][0], 'title' : tags["title"][0], 'artist' : tags["artist"][0], 'minutes' : minutes, 'seconds' : seconds, 'filePath' : y})
      except:
        unk = 'Unknown'
        newList.append({'trackId' : 0, 'album' : unk, 'title' : unk, 'artist' : unk, 'minutes' : minutes, 'seconds' : seconds, 'filePath' : y})
  print 'Deleting {0} Tracks'.format(len(deleteList))
  print 'Adding {0} Tracks'.format(len(newList))
  requests.post(apiUrl, json={'NewList':newList,'DeleteList':deleteList})
